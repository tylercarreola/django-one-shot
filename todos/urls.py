from django.urls import path
from todos.views import todo_list_list, todo_list_detail, todo_list_create, update_todo_list


urlpatterns  = [
    path("<int:id>/edit/", update_todo_list, name="update_todo_list"),
    path("create/", todo_list_create, name="todo_list_create"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("", todo_list_list, name="todo_list_list"),
]
