from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm

def todo_list_list(request):
    items = TodoList.objects.all()
    context = {
        "todo_list_list": items,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request,id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": todo_detail,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    context = {
        'form': form
    }
    return render(request, 'todos/create.html', context)


def update_todo_list(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect("todos_list_detail", id=todo_list.id)
    else:
        form = TodoListForm(instance=todo_list)

        context = {
            "form": form,
            "todo_list": todo_list,
    }
    return render(request, "todos/edit.html", context)

"""def todo_list_delete(request, id):
    todo_list_create = TodoList.objects.get(id=id)"""
